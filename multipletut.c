#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define CUT_TIME 1

sem_t stu;
sem_t tut;
//sem_t cord;
sem_t mutex;

int max_chairs = 0, max_tutors = 0, max_students = 0, max_help = 0;
int free_chairs = 0;
unsigned long int seat_pocket[]; 
static int count = 0;
int sit_next = 0;
int tutor_next = 0;

/*
void *coordinator(){

}
*/

void *waiting_room(void *temp){
    int my_chair, B;
    sem_wait(&mutex); //lock mutex to protect seat changes
    count++; //arrival of student
    printf("Student-%d [ID: %lu] entered waiting room\n", count, pthread_self());
    if(free_chairs > 0){
        --free_chairs;
        printf("Student-%d sits in waiting room\n", count);
        sit_next = (++sit_next) % max_chairs;
        my_chair = sit_next;
        seat_pocket[my_chair] = count;
        sem_post(&mutex); //release seat change mutex
        sem_post(&tut); // wake up a tutor
        sem_wait(&stu); //join queue of sleeping students
        sem_wait(&mutex); 
        B = seat_pocket[my_chair];  
        free_chairs++;  //release chair
        sem_post(&mutex); //release seat change mutex

    } else {
        sem_post(&mutex);
        printf("Student-%d finds no vacant seat and leaves\n", count);
    }
    pthread_exit(0);

}

void tutor_room(void *temp){
    int index = *(int*)(temp);
    printf("Tutor-%d [ID: %lu] joins csmc\n", index, pthread_self());
    int my_next, C;
    while(1){
            printf("Tutor-%d gone to sleep\n", index);
            sem_wait(&tut); //join queue of sleeping tutors
            sem_wait(&mutex);
            tutor_next = (++tutor_next)%max_chairs;
            C = seat_pocket[my_next];
            seat_pocket[my_next] = pthread_self();
            sem_post(&mutex);
            sem_post(&stu); //call a student
            printf("Tutor-%d wakes up and is cutting hair of Student-%d\n", index,C);
            sleep(CUT_TIME);
            printf("Tutor-%d finishes\n", index);
    }
}

int main(int argc, char*argv[]){
    if(argc != 4){
        printf("format: .c <total students> <total tutors> <total chairs> <max help>\n");
        exit(-1);       
    }

    max_students = atoi(argv[1]);
    max_tutors = atoi(argv[2]);
    max_chairs = atoi(argv[3]);
    max_help = atoi(argv[4]);
    free_chairs = max_chairs;

    pthread_t tutor[max_tutors];
    pthread_t student[max_students]; 
    //pthread_t coord;
    int i, status =0;
    
    /*Semaphore initialization*/
    sem_init(&stu, 0, 5);
    sem_init(&tut, 0, 0);
    //sem_init(&cord, 0, 1);
    sem_init(&mutex, 0, 1);

    /*
    //Coordinator Thread initialization
    printf("csmc opens\n");
    rc = pthread_create(&coord, NULL, coordinator, NULL);
    if(status!=0){
        stderr("Failed to create thread\n");
        //exit(-1);
    }*/

    /*tutor thread initialization*/
    printf("Tutors available now!\n");
    for(i=0; i<max_tutors; i++){
        status = pthread_create(&tutor[i], NULL, tutor_room, &i);
        if(status!=0){
            printf("Failed to create thread\n");
            //exit(-1);
        }
    }

    /*student thread initialization*/
    printf("Oh new students!\n");
    for(i=0; i<max_students; i++){
        status = pthread_create(&student[i], NULL, waiting_room, &i);
        if(status!=0){
            printf("Failed to create thread\n");
            //exit(-1);
        }
    }
   
   for(i=0; i<max_students; i++){
        pthread_join(student[i], NULL);
    }

    printf("csmc closes\n");
    exit(0);
}

