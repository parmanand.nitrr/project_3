#include<stdio.h>
#include<stdlib.h>

struct LinkedList
{   
    int student_id;
    int visits_count;
    struct LinkedList *next;
    struct LinkedList *prev;
};

typedef struct LinkedList *node;

node createNode(){
    node temp; 
    temp = (node)malloc(sizeof(struct LinkedList));
    temp->next = NULL;
    temp->prev = NULL;
    return temp;
}

node addNode(node head, int student_id,int visits_count){
    node swap,temp,p;// declare two nodes temp and p
    temp = createNode();//createNode will return a new node with data = value and next pointing to NULL.
    swap = createNode();
    temp->student_id = student_id; // add element's value to data part of node
    temp->visits_count = visits_count;
    if(head == NULL){
        head = temp;     //when linked list is empty
    }
    else{
        p  = head;//assign head to p 
        while(p->next != NULL){
            p = p->next;//traverse the list until p is the last node.The last node always points to NULL.
        }
        p->next = temp;//Point the previous last node to the new node created.
        temp->prev = p;
        swap =  p->prev;
    }
    return head;
}

node insertNodeWithPriority(node head, int student_id,int visits_count){
    node swap,temp,p;// declare two nodes temp and p
    temp = createNode();//createNode will return a new node with data = value and next pointing to NULL.
    swap = createNode();
    temp->student_id = student_id; // add element's value to data part of node
    temp->visits_count = visits_count;
    
    if(head == NULL){
        head = temp;     //when linked list is empty
    }
    else{
        p  = head;//assign head to p 
        while(p->next!= NULL){
            //traverse the list until p is the last node.The last node always points to NULL.
            //  printf("break student_id==> %d , visits==> %d\n",p->student_id,p->visits_count);
            if(temp->visits_count <= p->visits_count)
               break; 
        p = p->next;
        }
        
        if (p == NULL){
              p->next = temp;
              temp->prev = p;
              swap =  p->prev;
        }
        else{
            swap =  p->prev;
            p->prev = temp;//Point the previous last node to the new node created.
            temp->next = p;
            temp->prev = swap;
            temp->prev->next = temp;
        }
        
        
    }
    return head;
}

void printNode(node head){
      node p;
      p = head;
      while(p->next != NULL){
            p = p->next;//traverse the list until p is the last node.The last node always points to NULL.
            printf("student_id==> %d , visits==> %d",p->student_id,p->visits_count);
            printf("\n");
        }
}
node pop(node *head){
   node *temp = head;
   *head = (*head)->next;
   (*head)->prev =  NULL;
   return *temp;

}

int main(int argc, char const *argv[])
{
    
    node head = createNode();
    // addNode(head,1,2);
    // addNode(head,2,3);
    // addNode(head,3,4);
    insertNodeWithPriority(head,3,5);
    insertNodeWithPriority(head,5,0);
    insertNodeWithPriority(head,7,2);
    insertNodeWithPriority(head,8,7);
    insertNodeWithPriority(head,9,9);
    printNode(head);
    // node popped_node1 =  pop(&head);
    printf("++++++++++++++++++++++++++++++++++++++++++\n");
    printNode(head);
    // printf("popped stu id %d\n", (popped_node1)->student_id);
    printf("++++++++++++++++++++++++++++++++++++++++++\n");
    printNode(head);
    // node popped_node2 =  pop(&head);
    // printf("popped stu id %d\n", (popped_node2)->student_id);
    printf("+++++++++++++++++++++++++++++++insert new node+++++++++++\n");
    insertNodeWithPriority(head,16,0);
    printNode(head);
    printf("+++++++++++++++++++++++++++delete node+++++++++++\n");
    node popped_node3 =  pop(&head);
    printf("popped stu id %d\n", (popped_node3)->student_id);
    printNode(head);
    return 0;
}
